#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `python_boilerplate` package."""

import pytest


from python_boilerplate.python_boilerplate import hello



@pytest.mark.parametrize(
    "var,expected",
    [
        [
            'World!',
            'Hello World!',
        ],
        [
            'Universe!',
            'Hello Universe!',
        ]
    ],
)
def test_hello(var, expected):
    """
    Say hello to the world and universe
    """
    assert hello(var) == expected
