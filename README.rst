==================
Python Boilerplate
==================


.. image:: https://img.shields.io/pypi/v/python_boilerplate.svg
        :target: https://pypi.python.org/pypi/python_boilerplate
        :alt: pypi

.. image:: https://gitlab.com/reedrichards/python_boilerplate/badges/master/coverage.svg?job=coverage
        :target: https://reedrichards.gitlab.io/python_boilerplate/coverage/index.html
        :alt: coverage

.. image:: https://gitlab.com/reedrichards/python_boilerplate/badges/master/pipeline.svg
        :alt: pipeline

.. image:: https://img.shields.io/pypi/l/gitlab-helper.svg
        :target: https://gitlab.com/reedrichards/python_boilerplate/raw/master/LICENSE
        :alt: PyPI - License

.. image:: https://img.shields.io/pypi/dm/python_boilerplate.svg
        :alt: PyPI - Downloads

.. image:: https://img.shields.io/pypi/pyversions/python_boilerplate.svg
        :alt: PyPI - Python Version

.. image:: https://img.shields.io/pypi/status/python_boilerplate.svg
        :alt: PyPI - Status



Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: MIT license
* Documentation: https://reedrichards.gitlab.io/python_boilerplate/index.html
* Coverage: https://reedrichards.gitlab.io.gitlab.io/python_boilerplate/coverage/index.html
* Gitlab: https://gitlab.com/reedrichards/python_boilerplate


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
